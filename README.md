# ghosting_over_qemu

Info: https://wiki.archlinux.org/title/archiso#Installation

## Workflow

- `airootfs/etc/systemd/system/xfce4.service` (Symlinked to `airootfs/etc/systemd/system/multi-user.target.wants/xfce4.service`)
    - Start XFCE at system boot

- `airootfs/root/.config/autostart/ghost-vm.desktop`
    - Execute `start_ghost_vm.sh` script at XFCE launch

- `airootfs/root/start_ghost_vm.sh`  
    - Create a bridge.
    - Spoof real interface MAC address to VM && assign a random MAC to real interface.  
    - Passthrough a physical drive to VM and boot VM on PXE with predefined BIOS settings in `OVMF_VARS.fd`.

- `airootfs/root/OVMF_VARS.fd`
    - ~~Boot Order~~ (Fails when MAC address change):
        - ~~Network~~
        - ~~UEFI Shell~~
    - Screen Resolution:
        - 1920x1080

- `airootfs/root/netboot.iso`
    - ESP Bootload iPXE binary from ISO


Since in UEFI mode we have no possibility to change the Boot Order. And saved parameters in `OVMF_VARS.fd` fails when MAC address change, we decided to use `Bootable iPXE ISO`, because CDROM is always first on Boot Order.

## Credentials

### Dynamically generated
The build script generates a random password for the `root` user and `VNC server`. The password is stored build.env file.

### Static (just in information purposes)
Root password is set in `airootfs/etc/shadow` file. You can generate a new password with the command `openssl passwd -6` and then add it to the file.

```bash
...
root:hashed_password:14871::::::
...
```

VNC password is set in `airootfs/opt/qemu-ressources/x11vnc.pass` file. You can generate a new password with the command `vncpasswd -f` and then add it to the file.

```bash
echo MYVNCPASSWORD | vncpasswd -f > airootfs/opt/qemu-ressources/x11vnc.pass
```

## Build ISO
### Automated CI Build
The CI pipeline in this project automates the process of building the ISO image using GitLab CI/CD. Each commit triggers a pipeline that goes through several stages:

1. **Build Stage:** The `build_archiso.sh` script is executed to build the Arch Linux ISO image. The resulting ISO file is stored in cache to use in the next stage. The generated random password for the `root` user and `VNC server` is stored in the `build.env` file, in artifacts.
2. **Upload Stage:** The built ISO is uploaded to the GitLab Generic Package Registry. The pipeline uses the GitLab API to upload the ISO file to the Registry.
3. **Notify Stage:** After a successful build and upload, a notification is sent to the specified Discord channel, informing about the successful deployment along with the ISO download link. In case of failure, a failure notification is sent.

### Local Build
First clone `noVNC` git repository in `airootfs/opt/qemu-ressources/`
```bash
cd airootfs/opt/qemu-ressources
git clone https://github.com/novnc/noVNC.git --depth 1
```

Then build ISO image, go to project root and execute `build_archiso.sh`
```bash
sudo ./build_archiso.sh
```

You can also use a docker:
```bash
sudo docker run --name archiso --privileged -it --rm -v $PWD:/data archlinux:latest bash
cd /data
./build_archiso.sh
exit
```
