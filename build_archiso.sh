#!/usr/bin/env bash
: <<COMMENTBLOCK
title       :gen-archiso.sh
description :This script generates a custom Arch Linux ISO with specified packages.
author      :Valeriu Stinca
email       :ts@strategic.zone
date        :20230829
version     :v1
notes       :Modify the variables at the beginning of the script to customize the generated ISO.
=========================
COMMENTBLOCK

# Banner
echo "ICAgICAgIHwgICAgICAgICAgICAgICAgfCAgICAgICAgICAgICAgICBfKSAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAKICBfX3wgIF9ffCAgIF9ffCAgX2AgfCAgX198ICAgXyBcICAg
X2AgfCAgfCAgIF9ffCAgIF8gIC8gICBfIFwgICBfXyBcICAgIF8gXCAKXF9fIFwgIHwgICAgfCAg
ICAoICAgfCAgfCAgICAgX18vICAoICAgfCAgfCAgKCAgICAgICAgLyAgICggICB8ICB8ICAgfCAg
IF9fLyAKX19fXy8gXF9ffCBffCAgIFxfXyxffCBcX198IFxfX198IFxfXywgfCBffCBcX19ffCAg
IF9fX3wgXF9fXy8gIF98ICBffCBcX19ffCAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgIHxfX18vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAK" | base64 -d

set -e

# Define the base directory for Arch Linux live environment
ARCHLIVE_DIR="/tmp/archlive"

# Define the version and directories for work and output, relative to the base directory
VERSION="releng"
WORK_DIR="${ARCHLIVE_DIR}/work"
OUT_DIR="$PWD/out"

# Define the pattern for the ISO file located in the output directory
ISO_PATTERN=(${OUT_DIR}/ghosting_over_qemu-*.iso)

ZEROTIER_NETWORK_ID="159924d63096734b"
# setup hostname or use default
DEFAULT_HOSTNAME="sz-archkey"
# echo -n "Please set up the name of hostname or press Enter to use default ($DEFAULT_HOSTNAME): "
# read input_hostname
HOSTNAME=${ARCH_KEY_HOSTNAME:-$DEFAULT_HOSTNAME}

AUTHKEY="tskey-auth-kZoX2S2CNTRL-HRMUQ8ZtCndtK2CGCfUTmdJ3yHtreQRDd"

# Update Arch Linux keyring and refresh keys
pacman -Syy --noconfirm archlinux-keyring
pacman-key --refresh-keys

# Install Archiso
pacman -Syu --noconfirm archiso reflector rsync curl pwgen git tigervnc

ROOT_PASSWORD="$(pwgen -s 10 1)"
CRYPTED_PASSWORD="$(openssl passwd -6 $ROOT_PASSWORD)"
echo "HOSTNAME=${HOSTNAME}" | tee build.env
echo "ROOT_PASSWORD=${ROOT_PASSWORD}" | tee -a build.env

# Copy 'releng' configuration
cp -r /usr/share/archiso/configs/$VERSION/ $ARCHLIVE_DIR
cp pacman.conf profiledef.sh packages.x86_64 $ARCHLIVE_DIR/
cp -r airootfs $ARCHLIVE_DIR/
rm -rf $ARCHLIVE_DIR/efiboot/loader
cp -r efiboot $ARCHLIVE_DIR/

# Create symbolic links for systemd services
ln -sf /usr/lib/systemd/system/sshd.service $ARCHLIVE_DIR/airootfs/etc/systemd/system/
ln -sf /usr/lib/systemd/system/tailscaled.service $ARCHLIVE_DIR/airootfs/etc/systemd/system/
ln -sf /usr/lib/systemd/system/zerotier-one.service $ARCHLIVE_DIR/airootfs/etc/systemd/system/
ln -sf /usr/lib/systemd/system/nftables.service $ARCHLIVE_DIR/airootfs/etc/systemd/system/

# Create zerotier directory
mkdir -p $ARCHLIVE_DIR/airootfs/var/lib/zerotier-one/networks.d

# Create zerotier network join file
touch $ARCHLIVE_DIR/airootfs/var/lib/zerotier-one/networks.d/${ZEROTIER_NETWORK_ID}.conf

# Set root password in /etc/shadow
# Change root password in /etc/shadow
sed -i "s@^root:.*@root:$CRYPTED_PASSWORD:::::::@" $ARCHLIVE_DIR/airootfs/etc/shadow

# Modify mkinitcpio.conf to compress initramfs with zstd
# sed -i 's/^COMPRESSION.*$/COMPRESSION="zstd"/g' $ARCHLIVE_DIR/airootfs/etc/mkinitcpio.conf

# Modify SSH port
# Config is setted in sshd_config
# echo "Port 34522" >> $ARCHLIVE_DIR/airootfs/etc/ssh/sshd_config
# echo "AddressFamily any" >> $ARCHLIVE_DIR/airootfs/etc/ssh/sshd_config

# Create startup script
# echo '#!/usr/bin/env bash' > $ARCHLIVE_DIR/airootfs/root/startup.sh
echo 'tailscale up --authkey='$AUTHKEY >> $ARCHLIVE_DIR/airootfs/usr/local/bin/tailscale_up.sh
chmod +x $ARCHLIVE_DIR/airootfs/usr/local/bin/tailscale_up.sh

# Set hostname
echo $HOSTNAME > $ARCHLIVE_DIR/airootfs/etc/hostname

# add options to mkinitcpio
echo "BINARIES=(nvim)" | tee -a $ARCHLIVE_DIR/airootfs/etc/mkinitcpio.conf.d/archiso.conf
echo 'COMPRESSION="zstd"' | tee -a $ARCHLIVE_DIR/airootfs/etc/mkinitcpio.conf.d/archiso.conf
echo 'COMPRESSION_OPTIONS=(-15)' | tee -a $ARCHLIVE_DIR/airootfs/etc/mkinitcpio.conf.d/archiso.conf
echo 'MODULES=(i915 nvme crypto-crc32 intel_agp intel_lpss_pci intel_rst)' | tee -a $ARCHLIVE_DIR/airootfs/etc/mkinitcpio.conf.d/archiso.conf

# Clone noVNC repository to airootfs
git clone https://github.com/novnc/noVNC.git --depth 1 $ARCHLIVE_DIR/airootfs/opt/qemu-ressources/noVNC

# Set up x11vnc password
echo ${VNCPASSWORD:-$ROOT_PASSWORD} | vncpasswd -f > $ARCHLIVE_DIR/airootfs/opt/qemu-ressources/x11vnc.pass
echo "VNC_PASSWORD=${VNCPASSWORD:-$ROOT_PASSWORD}" | tee -a build.env

# Remove ${OUT_DIR} if it exists
if [ -d ${OUT_DIR} ]; then
    rm -rf ${OUT_DIR}
fi

# Build the ISO
mkarchiso -r -v -w ${WORK_DIR} -o ${OUT_DIR} ${ARCHLIVE_DIR}
sleep 5 && sync

# Check if there are any files that match the pattern
if [ ${#ISO_PATTERN[@]} -gt 0 ]; then
    echo "ISO generated successfully!"
    echo "Root password: $ROOT_PASSWORD"
    echo "VNC password: ${VNCPASSWORD:-$ROOT_PASSWORD}"
    echo "Hostname: $HOSTNAME"
    ISO_FILE="${ISO_PATTERN}"
    ls -alh ${ISO_FILE}
    if [ -f ${ISO_FILE} ]; then
        echo "ISO file: ${ISO_FILE}"
        echo "ISO_FILE=$(ls ${ISO_FILE})" | tee -a build.env
    fi
else
    echo "ISO generation failed!"
fi
# rm -rf "${ARCHLIVE_DIR}"
